Pod::Spec.new do |s|
  s.name         = "CoreMind"
  s.version      = "1.2"
  s.summary      = "topmindKit application related frameworks"
  s.homepage     = "https://www.topmind.eu"
  s.license      = "All rights reserved topmind GmbH"
  s.authors      = ["Martin Gratzer"]

  s.ios.deployment_target = "10.0"
  s.osx.deployment_target = "10.12"
  s.watchos.deployment_target = "4.0"
  s.tvos.deployment_target = "10.0"

  s.source       = {
    :git => "https://gitlab.com/topmindKit/topmindKit.git",
    :tag => "v#{s.version}"
  }
  s.source_files  = [ "topmindKit/#{s.name}/**/*.{h,m,swift}" ]
  s.exclude_files = [ "topmindKit/#{s.name}/Tests/**/*.{h,m,swift}" ]
  s.resources     = [ 'topmindKit/#{s.name}/**/*.{xib,storyboard,strings,xcassets}' ]
  s.frameworks    = [ 'Foundation' ]

end
