#!/bin/bash

#DESITNATION='platform=OS X, arch=x86_64'
DESTINATION='platform=iOS Simulator,name=iPhone 7'
SCHEME="TestMind"

xcodebuild \
    -workspace topmindKit.xcworkspace \
    -scheme "$SCHEME" \
    -destination "$DESTINATION" \
    test \
    | xcpretty
