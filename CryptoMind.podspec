Pod::Spec.new do |s|
  s.name         = "CryptoMind"
  s.version      = "1.2"
  s.summary      = "topmindKit application related frameworks"
  s.homepage     = "https://www.topmind.eu"
  s.license      = "All rights reserved topmind GmbH"
  s.authors      = ["Martin Gratzer"]
  
  s.ios.deployment_target = "10.0"
  s.osx.deployment_target = "10.12"
  s.watchos.deployment_target = "4.0"
  s.tvos.deployment_target = "10.0"

  s.requires_arc = true

  s.source       = {
    :git => "https://gitlab.com/topmindKit/topmindKit.git",
    :tag => "v#{s.version}"
  }
  s.source_files  = [ "topmindKit/#{s.name}/**/*.{h,m,swift}" ]
  s.exclude_files = [ "topmindKit/#{s.name}/Tests/**/*.{h,m,swift}" ]
  s.resources     = [ "topmindKit/#{s.name}/**/*.{xib,storyboard,strings,xcassets}" ]
  s.frameworks    = [ 'Foundation' ]
  
  s.preserve_paths = "topmindKit/#{s.name}/CommonCrypto/**/*"
  s.pod_target_xcconfig = {
    "SWIFT_INCLUDE_PATHS[sdk=macosx*]"           => "$(PODS_ROOT)/#{s.name}/topmindKit/#{s.name}/CommonCrypto/macosx",
    "SWIFT_INCLUDE_PATHS[sdk=iphoneos*]"         => "$(PODS_ROOT)/#{s.name}/topmindKit/#{s.name}/CommonCrypto/iphoneos",
    "SWIFT_INCLUDE_PATHS[sdk=iphonesimulator*]"  => "$(PODS_ROOT)/#{s.name}/topmindKit/#{s.name}/CommonCrypto/iphonesimulator",
    "SWIFT_INCLUDE_PATHS[sdk=appletvos*]"        => "$(PODS_ROOT)/#{s.name}/topmindKit/#{s.name}/CommonCrypto/appletvos",
    "SWIFT_INCLUDE_PATHS[sdk=appletvsimulator*]" => "$(PODS_ROOT)/#{s.name}/topmindKit/#{s.name}/CommonCrypto/appletvsimulator",
    "SWIFT_INCLUDE_PATHS[sdk=watchos*]"          => "$(PODS_ROOT)/#{s.name}/topmindKit/#{s.name}/CommonCrypto/watchos",
    "SWIFT_INCLUDE_PATHS[sdk=watchsimulator*]"   => "$(PODS_ROOT)/#{s.name}/topmindKit/#{s.name}/CommonCrypto/watchsimulator"
  }  
end
