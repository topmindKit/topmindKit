//
//  NetMind.h
//  NetMind
//
//  Created by Martin Gratzer on 23/10/2016.
//  Copyright © 2016 topmind mobile app solutions. All rights reserved.
//

@import Foundation;

//! Project version number for NetMind.
FOUNDATION_EXPORT double NetMindVersionNumber;

//! Project version string for NetMind.
FOUNDATION_EXPORT const unsigned char NetMindVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NetMind/PublicHeader.h>


