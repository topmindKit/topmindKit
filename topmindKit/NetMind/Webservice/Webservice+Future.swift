//
//  File.swift
//  NetMind
//
//  Created by Martin Gratzer on 04.03.19.
//

import Foundation
import CoreMind

extension Webserivce {

    public func send(method: Method, headers: [String: String]) -> Future<WebserviceResponse> {
        return Future<WebserviceResponse> {
            futureResult in
            send(method: method, headers: headers) { futureResult($0) }
        }
    }

    public func send<T: Decodable>(method: Method, headers: [String: String]) -> Future<T> {
        return Future<T> {
            futureResult in
            send(method: method, headers: headers) { futureResult($0) }
        }
    }
}
