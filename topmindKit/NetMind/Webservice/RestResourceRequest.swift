//
//  RestResourceRequest.swift
//  NetMind
//
//  Created by Martin Gratzer on 15.10.17.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import Foundation
import CoreMind

public enum RestResourceRequest<T: Codable, U: Equatable>: WebserviceRequest {

    case get(id: U)
    case create(T)
    case update(id: U, T)
    case delete(id: U)

    public var queryParameters: [String : String] {
        return [:]
    }

    public func encode(with format: WebserviceFormat) -> Result<Data>? {
        switch self {
        case let .create(resource), let .update(_, resource):
            return format.serialize(encodable: resource)

        default: // get / delete
            return nil
        }
    }

    public func decode<T>(response: WebserviceResponse, with format: WebserviceFormat) -> Result<T> where T : Decodable {
        return format.deserialize(decodable: response.data)
    }

    public var path: String {
        switch self {
        case let .get(id): return "\(id)"
        case let .delete(id): return "\(id)"
        case let .update(id, _): return "\(id)"
        default: return ""
        }
    }

    public var method: HttpMethod {
        switch self {
        case .get: return .get
        case .create: return .post
        case .update: return .put
        case .delete: return .delete
        }
    }    
}
