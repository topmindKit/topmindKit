//
//  AppDelegate.swift
//  topmindKitApp
//
//  Created by Martin Gratzer on 10/12/2016.
//  Copyright © 2016 topmind mobile app solutions. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

