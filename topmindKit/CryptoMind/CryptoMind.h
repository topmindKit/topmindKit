//
//  CryptoMind.h
//  CryptoMind
//
//  Created by Martin Gratzer on 10/12/2016.
//  Copyright © 2016 topmind mobile app solutions. All rights reserved.
//

@import Foundation;

//! Project version number for CryptoMind.
FOUNDATION_EXPORT double CryptoMindVersionNumber;

//! Project version string for CryptoMind.
FOUNDATION_EXPORT const unsigned char CryptoMindVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CryptoMind/PublicHeader.h>


