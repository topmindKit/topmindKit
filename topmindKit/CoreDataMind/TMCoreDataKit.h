//
//  CoreDataMind.h
//  CoreDataMind
//
//  Created by Martin Gratzer on 05/10/2016.
//  Copyright © 2016 topmind mobile app solutions. All rights reserved.
//

@import Foundation;

//! Project version number for CoreDataMind.
FOUNDATION_EXPORT double CoreDataMindVersionNumber;

//! Project version string for CoreDataMind.
FOUNDATION_EXPORT const unsigned char CoreDataMindVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreDataMind/PublicHeader.h>


