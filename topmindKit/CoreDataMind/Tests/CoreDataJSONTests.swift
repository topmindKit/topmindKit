//
//  CoreDataJSONTests.swift
//  topmindKit
//
//  Created by Peter Benkö on 30/12/2016.
//  Copyright © 2016 topmind mobile app solutions. All rights reserved.
//

import XCTest
import CoreMind
import CoreData
@testable import CoreDataMind

extension Kitten: JSONDecodable {
    func update(with json: JSONObject) throws {
        self.name = try json.decode(key: "name")
    }
}

class CoreDataJSONTests: CoreDataTests {
    
    func testDecoding() {
        let json: [String: Any] = [
            "name": "Tom"
        ]
        let jsonTom: Kitten? = try? decode(json, context: stack!.mainContext)
        
        XCTAssertEqual(jsonTom?.name, "Tom")
    }
    
    func testObjectArrayDecoding() {
        let json: [String: Any] = [ "array": [
            ["name": "Tom"],
            ["name": "Jerry"]
            ]
        ]
        
        do {
            let kittens: [Kitten] = try decode(json, key: "array", context: stack!.mainContext)
            XCTAssertEqual(kittens.map { $0.name }, ["Tom", "Jerry"])
        } catch {
            XCTFail("\(error)")
        }
    }
    
    func testDecodingWithIncorrectValue() {
        let json: [String: Any] = [
            "name": 1234
        ]
        
        do {
            let _: Kitten = try decode(json, context: stack!.mainContext)
            XCTFail("Should fail")
        } catch {
            // all good
        }
    }
    
    func testObjectArrayDecodingNonStrictShouldIgnoreIncorrectArrayType() {
        let json: [String: Any] = [ "array": NSNull() ]
        
        do {
            let dummy: [Kitten] = try decode(json, key: "array", context: stack!.mainContext)
            XCTAssertEqual(dummy, [])
        } catch {
            XCTFail("\(error)")
        }
    }
    
    func testObjectDeletionAfterCreationWhenInitUnsuccessful() {
        let json: [String: Any] = [
            "foo": "bar"
        ]
        
        do {
            let _: Kitten = try decode(json, context: stack!.mainContext)
            XCTFail("Should fail")
        } catch {
            XCTAssertTrue(stack!.mainContext.insertedObjects.first!.isDeleted)
        }
    }
    
}
