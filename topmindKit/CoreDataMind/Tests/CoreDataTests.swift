//
//  CoreDataTests.swift
//  CoreDataMindTests
//
//  Created by Martin Gratzer on 30/09/15.
//  Copyright © 2015 topmind mobile app solutions. All rights reserved.
//

import XCTest
import CoreData
@testable import CoreDataMind

@objc(Kitten)
class Kitten: NSManagedObject {
    @NSManaged var name: String
}

class CoreDataTests: XCTestCase {

    var kittens: CoreDataFetcher<Kitten>!
    var stack: CoreDataStack?
    lazy var modelUrl: URL? = {
        return Bundle(for: ContextObserverTests.self).url(forResource: "Model", withExtension: "momd")
    }()

    var tom: Kitten?
    var jerry: Kitten? // technically a mouse, but serves the purpose for now

    override func setUp() {
        super.setUp()
        if let url = modelUrl {
            let expect = expectation(description: "store init")
            stack = CoreDataStack(type: .sqlite, modelUrl: url) { _ in
                expect.fulfill()
            }
            waitForExpectations(timeout: 5, handler: nil)

            let context = stack!.mainContext

            kittens = CoreDataFetcher<Kitten>(context: context)

            tom = createKittenWithName("Tom")
            jerry = createKittenWithName("Jerry")

            XCTAssertDoesNotThrow(stack!.save(context: context, completion: { _ in }))
        }
    }

    override func tearDown() {
        if let storeUrl = stack?.storeURL {
            do {
                try FileManager.default.removeItem(at: storeUrl)
            } catch {
                XCTFail()
            }
        }
        stack = nil
        super.tearDown()
    }

    func createKittenWithName(_ name: String) -> Kitten? {
        let result = kittens.create { $0.name = name }
        switch result {
            case .success(let kitten):
            return kitten

            case .failure(let error):
            XCTFail(error.localizedDescription)
            return nil
        }
    }
}
