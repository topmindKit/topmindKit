//
//  json.swift
//  topmindKit
//
//  Created by Martin Gratzer on 30/12/2016.
//  Copyright © 2016 topmind mobile app solutions. All rights reserved.
//

import Foundation
import CoreData
import CoreMind

public extension JSONDecodable where Self: NSManagedObject {
    init(json: JSONObject, context: NSManagedObjectContext) throws {

        self.init(context: context)

        do {
            try update(with: json)
        } catch {
            context.delete(self)
            throw error
        }
    }
}

public func decode<T: JSONDecodable>(_ json: JSONObject, context: NSManagedObjectContext) throws -> T
    where T: NSManagedObject {
        return try T.init(json: json, context: context)
}

public func decode<T: JSONDecodable>(_ json: JSONObject, key: String, context: NSManagedObjectContext) throws -> [T]
    where T: NSManagedObject {
        guard let array = json[key] as? [JSONObject] else {
            return []
        }

        return array.compactMap { try? decode($0, context: context) }
}
