//
//  AppMind.h
//  AppMind
//
//  Created by Martin Gratzer on 02/09/2016.
//  Copyright © 2016 topmind mobile app solutions. All rights reserved.
//

@import Foundation;

#if TARGET_OS_MAC && !TARGET_OS_IPHONE
@import Cocoa;
#else
@import UIKit;
#endif

//! Project version number for AppMind.
FOUNDATION_EXPORT double AppMindVersionNumber;

//! Project version string for AppMind.
FOUNDATION_EXPORT const unsigned char AppMindVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AppMind/PublicHeader.h>


