//
//  ViewModelTests.swift
//  topmindKit
//
//  Created by Martin Gratzer on 02/09/2016.
//  Copyright © 2016 topmind mobile app solutions. All rights reserved.
//

import XCTest
import CoreMind
@testable import AppMind

// Usage

// 1. define your own observer type for special callbacks (not required)
protocol MyViewModelObserver: ViewModelObserver {
}

// 2. subclass ViewModel<ViewModelObserver>
final class MyViewModel: ViewModel<MyViewModelObserver> {
    func magic() {
        // do magic
        signalUpdate()
    }

    func signalUpdate() {
        observers.forEach { $0.didUpdate() }
    }
}

// 3. implement iplement the observer and observe the view model
final class ViewController: MyViewModelObserver {

    var numberOfDidUpdateCalls = 0
    var viewModel = MyViewModel()

    func didUpdate() {
        numberOfDidUpdateCalls += 1
    }
}

class ViewModelTests: XCTestCase {

    var vc: ViewController!
    var sut: MyViewModel!

    func testShouldNotCallDidUpdate() {
        givenViewController()

        whenViewModelIsChanged()

        XCTAssertEqual(vc.numberOfDidUpdateCalls, 0)
    }

    func testShouldCallDidUpdate() {
        givenViewController()

        whenObserving()
        whenViewModelIsChanged()

        XCTAssertEqual(vc.numberOfDidUpdateCalls, 1)
    }

    func testShouldCallDidUpdateMultipleTimes() {
        givenViewController()

        whenObserving()
        whenViewModelIsChanged()
        whenViewModelIsChanged()
        whenViewModelIsChanged()

        XCTAssertEqual(vc.numberOfDidUpdateCalls, 3)
    }

    func testShouldStopCallingDidUpdate() {
        givenViewController()

        whenObserving()
        whenViewModelIsChanged()
        whenStopObserving()
        whenViewModelIsChanged()

        XCTAssertEqual(vc.numberOfDidUpdateCalls, 1)
    }

    // MARK: Helper

    func givenViewController() {
        vc = ViewController()
        sut = vc.viewModel
    }

    func whenObserving() {
        vc.startObserving()
    }

    func whenStopObserving() {
        vc.stopObserving()
    }

    func whenViewModelIsChanged() {
        sut.magic()
    }
}
