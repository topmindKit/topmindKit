//
//  LandscapeSupporting.swift
//  AppMind
//
//  Created by Denis Andrašec on 27.08.19.
//  Copyright © 2019 topmind mobile app solutions. All rights reserved.
//

import Foundation

public protocol LandscapeSupporting: class {
    var landscapeEnabled: Bool { get }
}
