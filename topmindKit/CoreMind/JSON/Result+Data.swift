//
//  Result+JSON.swift
//  topmindKit
//
//  Created by Martin Gratzer on 30/12/2016.
//  Copyright © 2016 topmind mobile app solutions. All rights reserved.
//

import Foundation

extension Result {

    public func parse<U: Decodable>() -> Result<U> where T == Data {
        return Result<U> {
            let jsonData = try resolve()
            return try JSONDecoder().decode(U.self, from: jsonData)
        }
    }

    public func parse<U: Decodable>(key: String) -> Result<[U]> where T == Data {
        return Result<[U]> {
            let jsonData = try resolve()
            return try JSONDecoder().decode([U].self, from: jsonData)
        }
    }
}
