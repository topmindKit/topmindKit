//
//  Future.swift
//  topmindKit
//
//  Created by Martin Gratzer on 27/03/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import Foundation

public enum ResultQueue {
    case any, main, specific(DispatchQueue)

    public func execute(block: @escaping () -> ()) {
        guard let queue = self.queue else {
            block()
            return
        }
        queue.async(execute: block)
    }

    fileprivate var queue: DispatchQueue? {
        switch self {
        case .any:
            return nil

        case .specific(let q):
            return q

        case .main:
            return DispatchQueue.main
        }
    }
}

/// Future class executing "compute" exactly once
/// The calculated result is cached for later access
public final class Future<T> {

    private var disposable: Any? = nil
    private let resultQueue: ResultQueue
    private var data: Atomic<Data<T>> = Atomic(Data())

    public convenience init(resultQueue: ResultQueue = .any, value: T) {
        self.init(resultQueue: resultQueue, result: .success(value))
    }

    public init(resultQueue: ResultQueue = .any, result: Result<T>) {
        self.resultQueue = resultQueue
        notifyQueued(result)
    }

    public init(resultQueue: ResultQueue = .any, compute: (@escaping (Result<T>) -> Void) -> Void) {
        self.resultQueue = resultQueue
        compute(notifyQueued)
    }

    public init(resultQueue: ResultQueue = .any, compute: (@escaping (Result<T>) -> Void) -> Any?) {
        self.resultQueue = resultQueue
        disposable = compute(notifyQueued)
    }

    public static func join<T>(_ futures: [Future<T>], resultQueue: ResultQueue = .any) -> Future<[Result<T>]> {
        return Future<[Result<T>]>(resultQueue: resultQueue) {
            futureCompletion in

            let results = Atomic([Result<T>]())
            let group = DispatchGroup()
            for f in futures {
                group.enter()
                f.onResult {
                    result in

                    results.mutate { $0.append(result) }
                    group.leave()
                }
            }

            group.notify(queue: resultQueue.queue ?? .main) {
                futureCompletion(.success(results.value))
            }
        }
    }

    public func onResult(callback: @escaping (Result<T>) -> Void) {

        guard let cached = data.value.cached else {
            // remember callback for later execution
            data.mutate {
                $0.queuedCallbacks.append(callback)
            }
            return
        }

        // use previously calculated value
        notify(value: cached, to: [callback])
    }

    /// flatMap
    public func then<U>(_ next: @escaping (T) -> Future<U>) -> Future<U> {
        return Future<U> {
            completion in

            self.onResult {
                result in

                switch result {
                case .success(let value):
                    next(value).onResult(callback: completion)

                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }

    // map
    public func map<U>(_ transform: @escaping (T) -> U) -> Future<U> {
        return Future<U> {
            completion in
            onResult {
                completion($0.map(transform))
            }
        }
    }

    private func notifyQueued(_ value: Result<T>) {
        assert(data.value.cached == nil, "Don't call notify twice")

        var callbacks: [(Result<T>) -> Void] = []

        data.mutate {
            callbacks = $0.queuedCallbacks
            $0.cached = value
            $0.queuedCallbacks.removeAll()
        }

        notify(value: value, to: callbacks)
    }

    private func notify(value: Result<T>, to callbacks: [(Result<T>) -> Void]) {
        resultQueue.execute {
            callbacks.forEach { $0(value) }
        }
    }
}

extension Future {
    fileprivate final class Data<T> {
        var queuedCallbacks: [(Result<T>) -> Void] = []
        var cached: Result<T>? = nil
    }
}
