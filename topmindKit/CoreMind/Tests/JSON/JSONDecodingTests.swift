//
//  jsonTests.swift
//  topmindKit
//
//  Created by Martin Gratzer on 24/09/2016.
//  Copyright © 2016 topmind mobile app solutions. All rights reserved.
//

import XCTest
@testable import CoreMind

enum DummyIntEnum: Int {
    case A, B, C
}

enum DummyEnum: String {
    case A, B, C
}

struct Dummy: Equatable, JSONDeserializable {
    let name: String
    let createdAt: Date
    let url: URL?

    init(name: String, createdAt: Date, url: URL?) {
        self.name = name
        self.createdAt = createdAt
        self.url = url
    }

    init(json: JSONObject) throws {
        name = try json.decode(key: "name")
        createdAt = try json.decode(key: "created_at")

        do {
            let url: URL = try json.decode(key: "url")
            self.url = url
        } catch {
            url = nil
        }
    }

    static func ==(lhs: Dummy, rhs: Dummy) -> Bool {
        return lhs.name == rhs.name &&
            lhs.createdAt == rhs.createdAt &&
            lhs.url == rhs.url
    }
}

final class jsonTests: XCTestCase {

    let date = Date(timeIntervalSince1970: 1474576117)
    let dummy = Dummy(name: "Fixture",
                      createdAt: Date(timeIntervalSince1970: 1474576117),
                      url: URL(string: "http://www.topmind.eu"))

    func testStringArrayDeserialization() throws {
        let json: [String: Any] = ["array": ["A", "B", "C"]]

        let dummy: [String] = try  json.decode(key: "array")
        XCTAssertEqual(dummy, ["A", "B", "C"])
    }

    func testIntArrayDeserialization() throws {
        let json: [String: Any] = ["array": [1, 2, 3]]

        let dummy: [Int] = try json.decode(key: "array")
        XCTAssertEqual(dummy, [1, 2, 3])
    }

    func testDeserialization() throws {
        let json: [String: Any] = [
            "name": "Fixture",
            "created_at": "2016-09-22T22:28:37+02:00",
            "url": "http://www.topmind.eu"
        ]

        let sut: Dummy = try json.decode()

        XCTAssertEqual(sut, dummy)
    }

    func testDeserializationNested() throws {
        let json: [String: Any] = ["dummy": [
            "name": "Fixture",
            "created_at": "2016-09-22T22:28:37+02:00",
            "url": "http://www.topmind.eu"
            ]
        ]

        let sut: Dummy = try json.decode(key: "dummy")

        XCTAssertEqual(sut, dummy)
    }

    func testUrlDeserialization() throws {
        let json: [String: Any] = [
            "url": "http://topmind.eu"
        ]

        let sut: URL = try json.decode(key: "url")

        XCTAssertEqual(sut, URL(string: "http://topmind.eu"))
    }
    
    func testUrlDeserializationWithEncodingIssues() throws {
        let json: [String: Any] = [
            "url": "http://topmind.eu/this is a bad path/"
        ]
        
        let sut: URL = try json.decode(key: "url")
        
        XCTAssertEqual(sut, URL(string: "http://topmind.eu/this%20is%20a%20bad%20path/"))
    }

    func testIntDeserialization() throws {
        let json: [String: Any] = [
            "num": 123.321
        ]

        let sut: Int = try json.decode(key: "num")
        XCTAssertEqual(123, sut)
    }

    func testUIntDeserialization() throws {
        let json: [String: Any] = [
            "num": 123.321
        ]

        let sut: UInt = try json.decode(key: "num")
        XCTAssertEqual(123, sut)
    }

    func testInt8Deserialization() throws {
        let json: [String: Any] = [
            "num": 123.321
        ]

        let sut: Int8 = try json.decode(key: "num")
        XCTAssertEqual(123, sut)
    }

    func testUInt8Deserialization() throws {
        let json: [String: Any] = [
            "num": 123.321
        ]

        let sut: UInt8 = try json.decode(key: "num")
        XCTAssertEqual(123, sut)
    }


    func testInt16Deserialization() throws {
        let json: [String: Any] = [
            "num": 123.321
        ]
        
        let sut: Int16 = try json.decode(key: "num")
        
        XCTAssertEqual(123, sut)
    }

    func testUInt16Deserialization() throws {
        let json: [String: Any] = [
            "num": 123.321
        ]

        let sut: UInt16 = try json.decode(key: "num")

        XCTAssertEqual(123, sut)
    }
    
    func testInt32Deserialization() throws {
        let json: [String: Any] = [
            "num": 1234.3214
        ]
        
        let sut: Int32 = try json.decode(key: "num")
        
        XCTAssertEqual(1234, sut)
    }

    func testUInt32Deserialization() throws {
        let json: [String: Any] = [
            "num": 1234.3214
        ]

        let sut: UInt32 = try json.decode(key: "num")

        XCTAssertEqual(1234, sut)
    }

    func testInt64Deserialization() throws {
        let json: [String: Any] = [
            "num": 123456.321456
        ]

        let sut: Int64 = try json.decode(key: "num")

        XCTAssertEqual(123456, sut)
    }

    func testUInt64Deserialization() throws {
        let json: [String: Any] = [
            "num": 123456.321456
        ]

        let sut: UInt64 = try json.decode(key: "num")

        XCTAssertEqual(123456, sut)
    }
    
    func testFloatDeserializationWithWholeValue() throws {
        let json: [String: Any] = [
            "num": 123.321456
        ]
        
        let sut: Float = try json.decode(key: "num")
        
        XCTAssertEqual(123.321456, sut)
    }
    
    func testFloatDeserializationWithDoubleValue() throws {
        let json: [String: Any] = [
            "num": 123456.1234567891
        ]
        
        let sut: Float = try json.decode(key: "num")
        
        XCTAssertEqual(123456.1234567891, sut)
    }
    
    func testDoubleDeserializationWithWholeValue() {
        let json: [String: Any] = [
            "num": 123.321456
        ]
        
        let sut: Double? = try? json.decode(key: "num")
        
        XCTAssertEqual(123.321456, sut)
    }
    
    func testDoubleDeserializationWithDoubleValue() throws {
        let json: [String: Any] = [
            "num": 123456.1234567891
        ]
        
        let sut: Double = try json.decode(key: "num")
        
        XCTAssertEqual(123456.1234567891, sut)
    }

    func testOptionalDeserializationWithIncorrectValue() throws {
        let json: [String: Any] = [
            "name": "Fixture",
            "created_at": "2016-09-22T22:28:37+02:00",
            "url": ""
        ]

        let dummy: Dummy = try json.decode()

        XCTAssertNotNil(dummy)
        XCTAssertNil(dummy.url)
    }

    func testOptionalDeserializationWithMissingValue() throws {
        let json: [String: Any] = [
            "name": "Fixture",
            "created_at": "2016-09-22T22:28:37+02:00"
        ]

        let dummy: Dummy = try json.decode()

        XCTAssertNotNil(dummy)
        XCTAssertNil(dummy.url)
    }

    func testObjectArrayDeserialization() throws {
        let json: [String: Any] = [ "array": [
                        ["name": "Fixture 1", "created_at": "2016-09-22T22:28:37+02:00"],
                        ["name": "Fixture 2", "created_at": "2016-09-22T22:28:37+02:00"]
                    ]
        ]

        let dummy: [Dummy] = try json.decode(key: "array")
        XCTAssertEqual(dummy.map { $0.name }, ["Fixture 1", "Fixture 2"])
    }

    func testObjectArrayDeserializationStrictShouldFail() {
        let json: [String: Any] = [ "array": [
            ["created_at": "2016-09-22T22:28:37+02:00"],
            ["name": "Fixture 2", "created_at": "2016-09-22T22:28:37+02:00"]
            ]
        ]

        do {
            let _: [Dummy] = try json.decodeStrict(key: "array")
            XCTFail("Strict parsing should fail")
        } catch {
            // all good
        }
    }

    func testObjectArrayDeserializationNonStrictShouldIgnoreFailing() throws {
        let json: [String: Any] = [ "array": [
            [ "created_at": "2016-09-22T22:28:37+02:00"],
            ["name": "Fixture 2", "created_at": "2016-09-22T22:28:37+02:00"]
            ]
        ]

        let dummy: [Dummy] = try json.decode(key: "array")
        XCTAssertEqual(dummy.map { $0.name }, ["Fixture 2"])
    }

    func testObjectArrayDeserializationNonStrictShouldIgnoreIncorrectArrayType() throws {
        let json: [String: Any] = [ "array": NSNull() ]

        let dummy: [Dummy] = try json.decode(key: "array")
        XCTAssertEqual(dummy, [])
    }

    func testStringEnumDeserialization() throws {
        let json: [String: Any] = ["enumvalue": "A"]

        let dummy: DummyEnum = try json.decode(key: "enumvalue")
        XCTAssertEqual(dummy, .A)
    }

    func testStringEnumArrayDeserialization() throws {
        let json: [String: Any] = ["enumvalues": ["A", "B", "C"]]

        let dummy: [DummyEnum] = try json.decode(key: "enumvalues")
        XCTAssertEqual(dummy, [.A, .B, .C])
    }

    func testIntEnumDeserialization() throws {
        let json: [String: Any] = ["enumvalue": 0]

        let dummy: DummyIntEnum = try json.decode(key: "enumvalue")
        XCTAssertEqual(dummy, .A)
    }

    func testIntEnumArrayDeserialization() throws {
        let json: [String: Any] = ["enumvalues": [0, 1, 2]]

        let dummy: [DummyIntEnum] = try json.decode(key: "enumvalues")
        XCTAssertEqual(dummy, [.A, .B, .C])
    }

    func testISO8601() throws {
        let json: [String: Any] = [
            "timezone": "2016-09-22T22:28:37+02:00",
            "utc": "2016-09-22T20:28:37Z",
            "skiline": "2016-09-22T22:28:37.000+02:00"
        ]

        XCTAssertEqual(try json.decode(key: "timezone"), date)
        XCTAssertEqual(try json.decode(key: "utc"), date)
    }

    func testDateWithCustomFormat() throws {
        let json: JSONObject = [
            "timezone": "2016-09-22T22:28:37+02:00",
            "utc": "2016-09-22T20:28:37Z",
            "skiline": "2016-09-22T22:28:37.000+02:00"
        ]

        XCTAssertEqual(try json.decode(key: "timezone"), date)
        XCTAssertEqual(try json.decode(key: "utc"), date)
        XCTAssertEqual(try json.decode(key: "skiline", format: "yyyy-MM-dd'T'HH:mm:ss.000XXXXX"), date)
    }

    func testUnixTimestamp() throws {
        let json: [String: Any] = [
            "timestamp": 1474576117
        ]
        
        XCTAssertEqual(try json.decode(key: "timestamp"), date)
    }
}
