//
//  PurchaseCoordinator.swift
//  StoreMind
//
//  Created by Martin Gratzer on 07/07/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import Foundation
import CoreMind

public protocol PurchaseCoordinatorObserver: Observer {
    func didPurchase(result: Verification)
    func didRestore(with error: Error?)
}

public final class PurchaseCoordinator {

    fileprivate let observers = MulticastDelegate<PurchaseCoordinatorObserver>()

    public enum Error: Swift.Error {
        case unknown
    }

    public let client: StoreClient
    public let verifier: PurchaseVerifier

    fileprivate var products = Atomic<[ProductIdentifier: Product]>([:])

    public convenience init(verifier: PurchaseVerifier) {
        self.init(client: AppStoreClient(), verifier: verifier)
    }

    public init(client: StoreClient, verifier: PurchaseVerifier) {
        self.client = client
        self.verifier = verifier
        self.client.delegate = self
    }

    // MARK: Products

    public func product(for identifier: ProductIdentifier) -> Product? {
        return products.value[identifier]
    }

    public func loadProducts(for productIdentifier: [ProductIdentifier], completion: (@escaping (Result<[Product]>) -> ())) {

        guard !productIdentifier.isEmpty else {
            completion(.success([]))
            logWarning("Ignored product loading request with empty `productIdentifier` list.", tag: "PurchaseCoordinator")
            return
        }

        // use cached prices
        let products = productIdentifier
            .compactMap { self.products.value[$0] }
            .filter { productIdentifier.contains($0.id) }

        if products.count == productIdentifier.count {
            completion(.success(products))
        }

        client.loadProducts(for: productIdentifier) {
            [unowned self] in
            self.cacheProducts(result: $0)
            completion($0)
        }
    }

    // MARK: Receipt

    public func refreshReceipt(completion: @escaping (Swift.Error?) -> Void) {
        client.refreshReceipt(completion: completion)
    }

    // MARK: Purchase

    public func purchase(productWith identifier: ProductIdentifier, userIdentifier: String?) throws {
        try client.purchase(productWith: identifier, userIdentifier: userIdentifier)
    }

    public func restorePurchases() throws {
        try client.restore()
    }

    public func add(observer: PurchaseCoordinatorObserver) {
        observers += observer
    }

    public func remove(observer: PurchaseCoordinatorObserver) {
        observers -= observer
    }
}

extension PurchaseCoordinator: StoreClientDelegate {

    public func purchaseResult(_ result: TransactionResult) {

        guard !result.allFailed else {
            let v = Verification(transaction: result, verification: .unknownFailure(result.failed.first ?? Error.unknown))
            observers.invoke { $0.didPurchase(result: v) }
            client.finishTransactions(result: result)
            return
        }

        verifier.verifyReceipt(with: result) {
            [weak self] verification in

            switch verification.verification {
            case .success:
                self?.client.finishTransactions(result: verification.transaction)

            case .validationFailed:
                self?.client.finishTransactions(result: verification.transaction)

            // causes validation retry
            case .unknownFailure:
                break
            }

            self?.observers.invoke { $0.didPurchase(result: verification) }
        }
    }

    public func restoreResult(_ result: Result<Void>) {
        guard let error = result.error else {
            observers.invoke { $0.didRestore(with: nil) }
            return
        }
        observers.invoke { $0.didRestore(with: error) }
    }
}

extension PurchaseCoordinator {
    fileprivate func cacheProducts(result: Result<[Product]>) {
        for product in result.value ?? [] {
            products.mutate { $0[product.id] = product }
        }
    }
}
