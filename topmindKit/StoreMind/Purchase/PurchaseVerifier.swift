//
//  PurchaseVerifier.swift
//  StoreMind
//
//  Created by Martin Gratzer on 07/07/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import Foundation

public protocol PurchaseVerifier {
    func verifyReceipt(with result: TransactionResult, completion: @escaping (Verification) -> ())
}
