//
//  StoreClient.swift
//  StoreMind
//
//  Created by Martin Gratzer on 01/07/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import Foundation
import CoreMind

public typealias ProductIdentifier = String

public protocol StoreClient: class {

    var delegate: StoreClientDelegate? { get set }
    
    var canMakePayments: Bool { get }

    func refreshReceipt(completion: @escaping (Swift.Error?) -> Void)
    func loadProducts(for identifiers: [ProductIdentifier], completion: @escaping (Result<[Product]>) -> Void)

    func purchase(productWith identifier: ProductIdentifier, userIdentifier: String?) throws
    func finishTransactions(result: TransactionResult)
    func restore() throws    
}

public protocol StoreClientDelegate: class {
    func purchaseResult(_ result: TransactionResult)
    func restoreResult(_ result: Result<Void>)
}

public struct TransactionResult {
    public let token = UUID()
    public let purchased: [Purchase]
    public let restored: [Purchase]
    public let failed: [Purchase.Error]

    public init(purchased: [Purchase], restored: [Purchase], failed: [Purchase.Error]) {
        self.purchased = purchased
        self.restored = restored
        self.failed = failed
    }

    public var isEmpty: Bool {
        return purchased.isEmpty && restored.isEmpty && failed.isEmpty
    }

    public var allFailed: Bool {
        return purchased.isEmpty && restored.isEmpty && !failed.isEmpty
    }
}

public struct Verification {

    public enum Result {
        case success(Any)
        /// Failed validation like invalid receipt (this error will finish pruchase transactions)
        case validationFailed(Error)
        /// All other errors  (this error will NOT finish pruchase transactions and verification will be retried)
        case unknownFailure(Error)
    }

    public let transaction: TransactionResult
    public let verification: Result

    public init(transaction: TransactionResult, verification: Result) {
        self.transaction = transaction
        self.verification = verification
    }
}
