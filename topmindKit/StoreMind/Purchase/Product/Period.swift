//
//  Period.swift
//  StoreMind
//
//  Created by Martin Gratzer on 11.03.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import Foundation

public extension Product {
    struct Period {
        public enum Unit {
            case day
            case week
            case month
            case year
            case forever
        }
        public let unit: Unit
        public let numberOfUnits: UInt

        internal init(unit: Unit, numberOfUnits: UInt) {
            assert(numberOfUnits > 0)
            self.unit = unit
            self.numberOfUnits = unit == .forever ? 1 : numberOfUnits
        }

        public static let forever = Period(unit: .forever, numberOfUnits: 1)
    }
}

// These extensions should be in AppStore+Extensions.swift
// I have no idea why they dont work there as extnesions for Product.Discount do?
// TODO: Try to move with next Swift version
import StoreKit

@available(iOS 11.2, *)
extension Product.Period {
    init(_ subscriptionPeriod: SKProductSubscriptionPeriod) {
        self.init(unit: .init(subscriptionPeriod.unit),
                  numberOfUnits: UInt(subscriptionPeriod.numberOfUnits))
    }
}

@available(iOS 11.2, *)
extension Product.Period.Unit {
    init(_ unit: SKProduct.PeriodUnit) {
        switch unit {
        case .day: self = .day
        case .week: self = .week
        case .month: self = .month
        case .year: self = .year
        @unknown default:
            self = .forever
        }
    }
}
