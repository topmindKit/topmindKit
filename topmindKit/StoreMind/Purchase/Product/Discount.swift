//
//  Discount.swift
//  StoreMind
//
//  Created by Martin Gratzer on 11.03.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import Foundation

public extension Product {
    struct Discount {
        public enum PaymentMode {
            case payAsYouGo
            case payUpFront
            case freeTrial
            case unknown
        }

        public let price: Decimal
        public let localizedPrice: String?
        /// The price in millionths of the currency base unit
        public let priceMicros: Decimal
        public let paymentMode: PaymentMode
        public let numberOfPeriods: UInt
        public let period: Period

        internal init(price: Decimal, paymentMode: PaymentMode, numberOfPeriods: UInt, period: Period, locale: Locale) {
            self.price = price
            self.priceMicros = price * 1_000_000
            self.period = period
            self.numberOfPeriods = numberOfPeriods
            self.paymentMode = paymentMode

            let formatter = Product.priceFormatter(for: locale)
            self.localizedPrice = formatter.string(from: NSDecimalNumber(decimal: price))
        }
    }
}

// Extension should be placed in AppStore+Extensions.swift, compiler is not happy :-(

import StoreKit

@available(iOS 11.2, *)
extension Product.Discount {
    init(_ discount: SKProductDiscount) {
        self.init(price: discount.price.decimalValue,
                  paymentMode: .init(discount.paymentMode),
                  numberOfPeriods: UInt(discount.numberOfPeriods),
                  period: .init(discount.subscriptionPeriod),
                  locale: discount.priceLocale)
    }
}

@available(iOS 11.2, *)
extension Product.Discount.PaymentMode {
    init(_ paymentMethod: SKProductDiscount.PaymentMode) {
        switch paymentMethod {
        case .payAsYouGo: self = .payAsYouGo
        case .payUpFront: self = .payUpFront
        case .freeTrial: self = .freeTrial
        @unknown default:
            assertionFailure("Unknown payment method \(paymentMethod)")
            self = .unknown
        }
    }
}
