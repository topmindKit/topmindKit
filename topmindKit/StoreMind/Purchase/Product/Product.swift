//
//  Product.swift
//  StoreMind
//
//  Created by Martin Gratzer on 01/07/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import Foundation

public struct Product: Hashable {

    public let id: ProductIdentifier

    public let title: String
    public let description: String

    public let price: Decimal
    public let localizedPrice: String?
    public let currencyCode: String?
    /// The price in millionths of the currency base unit
    public let priceMicros: Decimal
    public let subscriptionPeriod: Period?
    public let discount: Discount?

    private let locale: Locale

    internal init(id: ProductIdentifier, title: String, description: String, price: Decimal, subscriptionPeriod: Period?, discount: Discount?, locale: Locale) {
        self.id = id
        self.title = title
        self.description = description
        self.price = price
        self.currencyCode = locale.currencyCode
        self.priceMicros = price * 1_000_000

        self.subscriptionPeriod = subscriptionPeriod
        self.discount = discount

        self.locale = locale

        let formatter = Product.priceFormatter(for: locale)
        self.localizedPrice = formatter.string(from: NSDecimalNumber(decimal: price))
    }

    public func localizedPrice(dividedBy: Int) -> String? {
        let roundedPrice = price(dividedBy: dividedBy)
        return Product.priceFormatter(for: locale).string(from: NSDecimalNumber(value: roundedPrice))
    }

    public func price(dividedBy: Int) -> Double {
        let divider = max(1.0, Double(abs(dividedBy)))
        let dividedPrice = (NSDecimalNumber(decimal: price).doubleValue / divider)
        return floor(dividedPrice * 100.0) / 100.0
    }

    public static func ==(lhs: Product, rhs: Product) -> Bool {
        return lhs.id == rhs.id
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

extension Product {
    static var priceFormatters = [String: NumberFormatter]()
    static func priceFormatter(for locale: Locale) -> NumberFormatter {
        guard let formatter = priceFormatters[locale.identifier] else {
            let formatter = NumberFormatter()
            formatter.formatterBehavior = .behavior10_4
            formatter.numberStyle = .currencyAccounting
            formatter.usesGroupingSeparator = true
            formatter.locale = locale
            formatter.maximumFractionDigits = 2
            formatter.minimumFractionDigits = 2
            priceFormatters[locale.identifier] = formatter
            return formatter
        }
        return formatter
    }
}
