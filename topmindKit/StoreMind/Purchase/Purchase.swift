//
//  Purchase.swift
//  StoreMind
//
//  Created by Martin Gratzer on 01/07/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import Foundation

public struct Purchase: Hashable {
    
    public struct Transaction {
        // The unique server-provided identifier.
        // Only valid if state is SKPaymentTransactionStatePurchased or SKPaymentTransactionStateRestored.
        public let id: String?
        public let restored: Bool
        // The date when the transaction was added to the server queue.
        // Only valid if state is SKPaymentTransactionStatePurchased or SKPaymentTransactionStateRestored.
        public let date: Date?
    }
    
    public struct Error: Swift.Error {
        public let id: ProductIdentifier
        public let error: Swift.Error
    }
    
    public typealias Source = String
    
    public let product: ProductIdentifier
    public let quantity: Int
    public let transaction: Transaction

    public let id: UUID
    
    public init(product: ProductIdentifier,
                quantity: Int,
                transaction: Transaction) {
        self.product = product
        self.quantity = quantity
        self.transaction = transaction
        self.id = UUID()
    }
    
    public static func ==(lhs: Purchase, rhs: Purchase) -> Bool {
        return lhs.product == rhs.product && lhs.transaction.id == rhs.transaction.id
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(id.uuidString)
    }
}
