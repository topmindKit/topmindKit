//
//  AppStoreClient.swift
//  StoreMind
//
//  Created by Martin Gratzer on 01/07/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import Foundation
import CoreMind
import StoreKit

public final class AppStoreClient: NSObject, StoreClient {

    public enum Error: Swift.Error {
        case canNotmakePayments
        case productNotFound(ProductIdentifier)
        case productsNotFound([ProductIdentifier])
    }

    public weak var delegate: StoreClientDelegate?

    private var transactions = Atomic<[UUID: [SKPaymentTransaction]]>([:])
    private let queue = OperationQueue()
    
    public override init() {
        super.init()
        queue.qualityOfService = .userInitiated
        logInfo("SKPaymentQueue active", tag: "AppStoreClient")
        SKPaymentQueue.default().add(self)
    }

    deinit {
        queue.cancelAllOperations()
        queue.waitUntilAllOperationsAreFinished()
        SKPaymentQueue.default().remove(self)
        logInfo("SKPaymentQueue disabled", tag: "AppStoreClient")
    }

    // MARK: ProductStore

    public var canMakePayments: Bool {
        return SKPaymentQueue.canMakePayments()
    }

    public func refreshReceipt(completion: @escaping (Swift.Error?) -> Void) {
        logInfo("Refreshing receipt", tag: "AppStoreClient")
        let operation = AppStoreReceiptRefreshOperation()
        operation.completion = completion
        queue.addOperation(operation)
    }

    public func loadProducts(for identifiers: [ProductIdentifier], completion: @escaping (Result<[Product]>) -> Void) {
        logInfo("Loading products for `\(identifiers)`.", tag: "AppStoreClient")
        let operation = AppStoreProductOperation(identifiers: identifiers) {
            completion($0.map { $0.products.map(Product.init) })
            if let invalid = $0.value?.invalidProductIdentifiers, !invalid.isEmpty {
                logError("Invalid Product Identifiers: \(invalid.joined(separator: ", "))", tag: "AppStoreClient")
            }
        }
        queue.addOperation(operation)
    }

    public func purchase(productWith identifier: ProductIdentifier, userIdentifier: String?) throws {

        guard canMakePayments else {
            logError("Payments disabled", tag: "AppStoreClient")
            throw Error.canNotmakePayments
        }

        let payment = SKMutablePayment()
        payment.productIdentifier = identifier
        if let userId = userIdentifier {
            payment.applicationUsername = userId
        }

        #if DEBUG
            /**
                 http://asciiwwdc.com/2016/sessions/702

                 "So if you want to test your deferred transaction in Sandbox, it can be done using SimulatesAskToBuyInSandbox.
                 This is where you create your SKMutable payment object.
                 You pass in that product and then you set the SimulatesAskToBuyInSandbox flag.
                 This is a flag that's going to tell the App Store, hey, treat this as if a child was buying this and that child was part of a family."
             */
//            payment.simulatesAskToBuyInSandbox = true
        #endif

        logInfo("Purchasing `\(identifier)`", tag: "AppStoreClient")
        SKPaymentQueue.default().add(payment)
    }

    public func restore() throws {

        guard canMakePayments else {
            logError("Payments disabled", tag: "AppStoreClient")
            throw Error.canNotmakePayments
        }

        logInfo("Restoring payments", tag: "AppStoreClient")
        SKPaymentQueue.default().restoreCompletedTransactions()
    }

    public func finishTransactions(result: TransactionResult) {

        guard let transacitons = transactions.value[result.token] else {
            return
        }

        let filtered = TransactionsFilter(transacitons)
        // finalize finnsihed transactions
        filtered.finished.forEach {
            SKPaymentQueue.default().finishTransaction($0)
        }

        transactions.mutate { $0.removeValue(forKey: result.token) }
    }

    // MARK: SKPaymentTransactionObserver

    // Sent when the transaction array has changed (additions or state changes).  Client should check state of transactions and finish as appropriate.
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        let filtered = TransactionsFilter(transactions)

        guard !filtered.isEmpty else {
            filtered.queued.forEach {
                logInfo("Queued `\($0.payment.productIdentifier)`", tag: "AppStoreClient")
            }
            return
        }

        filtered.purchased.forEach {
            logInfo("Purchased `\($0.payment.productIdentifier)`", tag: "AppStoreClient")
        }

        filtered.restored.forEach {
            logInfo("Restored `\($0.payment.productIdentifier)`", tag: "AppStoreClient")
        }

        filtered.failed.forEach {
            logInfo("Failed `\($0.payment.productIdentifier)`", tag: "AppStoreClient")
        }

        let result = TransactionResult(filter: filtered)
        self.transactions.mutate { $0[result.token] = transactions }

        delegate?.purchaseResult(result)
    }

    // Sent when transactions are removed from the queue (via finishTransaction:).
    public func paymentQueue(_ queue: SKPaymentQueue, removedTransactions transactions: [SKPaymentTransaction]) {
        // discuss: should we report results here?
        // let filtered = TransactionsFilter(transactions)
        logInfo("removedTransactions: \(transactions.map { $0.payment.productIdentifier })", tag: "AppStoreClient")
    }

    // Sent when an error is encountered while adding transactions from the user's purchase history back to the queue.
    public func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        delegate?.restoreResult(.failure(error))
    }

    // Sent when all transactions from the user's purchase history have successfully been added back to the queue.
    public func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        delegate?.restoreResult(.success(()))
    }

//    // Sent when the download state has changed.
//    public func paymentQueue(_ queue: SKPaymentQueue, updatedDownloads downloads: [SKDownload]) {
//
//    }
//
//    // Sent when a user initiates an IAP buy from the App Store
//    @available(iOS 11.0, *)
//    public func paymentQueue(_ queue: SKPaymentQueue, shouldAddStorePayment payment: SKPayment, for product: SKProduct) -> Bool {
//        return false
//    }

}

extension AppStoreClient: SKPaymentTransactionObserver {
    // Methods implemented in class -> not allowed here due to @objc requirements (Swift 3.2??)
}
