//
//  AppStoreProductOperation.swift
//  StoreMind
//
//  Created by Martin Gratzer on 01/07/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import Foundation
import CoreMind
import StoreKit

internal class AppStoreProductOperation: ConcurrentOperation, SKProductsRequestDelegate {

    internal struct ProductResult {
        var products: [SKProduct]
        var invalidProductIdentifiers: [ProductIdentifier]
    }

    internal let identifiers: Set<ProductIdentifier>
    private var products = [SKProduct]()
    private var invalidProductIdentifiers = [ProductIdentifier]()

    private var productRequest: SKProductsRequest?

    private var completion: ((Result<ProductResult>) -> Void)?

    public required init(identifiers: [ProductIdentifier], completion: @escaping ((Result<ProductResult>) -> Void)) {
        self.identifiers = Set(identifiers)
        self.completion = completion
        super.init()
    }

    override func main() {
        super.main()
        productRequest = SKProductsRequest(productIdentifiers: identifiers)
        productRequest?.delegate = self
        productRequest?.start()
    }

    override func cancel() {
        productRequest?.cancel()
        super.cancel()
    }

    // MARK: SKProductsRequestDelegate
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        products += response.products
        invalidProductIdentifiers += response.invalidProductIdentifiers
    }

    public func requestDidFinish(_ request: SKRequest) {
        completion?(.success( ProductResult(products: products, invalidProductIdentifiers: invalidProductIdentifiers) ))
        completion = nil
        finish(error: nil)
    }

    public func request(_ request: SKRequest, didFailWithError error: Swift.Error) {
        completion?(.failure(error))
        completion = nil
        finish(error: error)
    }
}

extension SKProductsRequest {
    internal convenience init(identifiers: [ProductIdentifier], delegate: SKProductsRequestDelegate) {
        self.init(productIdentifiers: Set(identifiers))
        self.delegate = delegate
    }
}
