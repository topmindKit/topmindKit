//
//  AppStoreReceiptRefreshOperation.swift
//  StoreMind
//
//  Created by Martin Gratzer on 07/07/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import Foundation
import CoreMind
import StoreKit

/// Concurrent Operation Encapsulating SKReceiptRefreshRequest
internal class AppStoreReceiptRefreshOperation: ConcurrentOperation, SKRequestDelegate {

    /**
        Receipt properties for sandbox testing:
        public let SKReceiptPropertyIsExpired: String // NSNumber BOOL, defaults to NO
        public let SKReceiptPropertyIsRevoked: String // NSNumber BOOL, defaults to NO
        public let SKReceiptPropertyIsVolumePurchase: String // NSNumber BOOL, defaults to NO
     */
    private(set) public var receiptProperties: [String: Any]?
    private var receiptRequest: SKReceiptRefreshRequest?

    public var completion: ((Swift.Error?) -> Void)?

    override func main() {
        super.main()
        receiptRequest = SKReceiptRefreshRequest(receiptProperties: receiptProperties)
        receiptRequest?.delegate = self
        receiptRequest?.start()
    }

    override func cancel() {
        receiptRequest?.cancel()
        super.cancel()
    }

    override func finish(error: Swift.Error?) {
        completion?(error)
        completion = nil
        receiptRequest = nil
        super.finish(error: error)
    }

    // MARK: SKRequestDelegate
    public func requestDidFinish(_ request: SKRequest) {
        finish(error: nil)
    }

    public func request(_ request: SKRequest, didFailWithError error: Swift.Error) {
        finish(error: error)
    }
}
