//
//  AppStore+Extensions.swift
//  StoreMind
//
//  Created by Martin Gratzer on 11.03.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import StoreKit

struct TransactionsFilter {
    let purchased: [SKPaymentTransaction]
    let restored: [SKPaymentTransaction]
    let failed: [SKPaymentTransaction]
    let queued: [SKPaymentTransaction]
    let finished: [SKPaymentTransaction]

    init(_ transactions: [SKPaymentTransaction]) {
        purchased = transactions.purchased
        restored = transactions.restored
        failed = transactions.failed
        queued = transactions.queued
        finished = transactions.finished
    }

    var isEmpty: Bool {
        return purchased.isEmpty && restored.isEmpty && failed.isEmpty && finished.isEmpty
    }
}

extension TransactionResult {

    public enum Error: Swift.Error {
        case unknown
    }

    init(filter: TransactionsFilter) {
        purchased = filter.purchased.purchases()
        restored = filter.restored.purchases()
        failed = filter.failed.map {
            Purchase.Error(id: $0.payment.productIdentifier,
                           error: $0.error ?? Error.unknown)
        }
    }
}

extension Purchase.Transaction {
    init(_ transaction: SKPaymentTransaction) {
        id = transaction.transactionIdentifier
        date = transaction.transactionDate
        restored = transaction.transactionState == .restored
    }
}

extension Product {
    init(_ product: SKProduct) {

        let period: Period?
        let discount: Discount?
        if #available(iOS 11.2, *) {
            period = product.subscriptionPeriod.map(Period.init) ?? .forever
            if #available(iOS 12.0, *) {
                discount = product.introductoryPrice.map(Discount.init)
            } else {
                discount = nil
            }
        } else {
            period = nil
            discount = nil
        }

        self.init(
            id: product.productIdentifier,
            title: product.localizedTitle,
            description: product.localizedDescription,
            price: product.price.decimalValue,
            subscriptionPeriod: period,
            discount: discount,
            locale: product.priceLocale
        )
    }
}

extension Collection where Iterator.Element == SKPaymentTransaction {

    /// Transaction is in queue, user has been charged.  Client should complete the transaction.
    var purchased: [SKPaymentTransaction] {
        return transactions(for: [.purchased])
    }

    /// Transaction was restored from user's purchase history.  Client should complete the transaction.
    var restored: [SKPaymentTransaction] {
        return transactions(for: [.restored])
    }

    /// Transaction was cancelled or failed before being added to the server queue.
    var failed: [SKPaymentTransaction] {
        return transactions(for: [.failed])
    }

    var finished: [SKPaymentTransaction] {
        return purchased + restored + failed
    }

    /// Transaction is being added to the server queue. (Its final status might pending external action.)
    var queued: [SKPaymentTransaction] {
        return transactions(for: [.deferred, .purchasing])
    }

    func purchases() -> [Purchase] {
        return compactMap {
            Purchase(product: $0.payment.productIdentifier,
                     quantity: $0.payment.quantity,
                     transaction: Purchase.Transaction($0))
        }
    }

    private func transactions(for states: [SKPaymentTransactionState]) -> [SKPaymentTransaction] {
        return filter {
            states.contains($0.transactionState)
        }
    }
}
