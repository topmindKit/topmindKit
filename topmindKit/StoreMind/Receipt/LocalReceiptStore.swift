//
//  LocalReceiptStore.swift
//  StoreMind
//
//  Created by Martin Gratzer on 01/07/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import Foundation
import CoreMind

public final class LocalReceiptStore: ReceiptStore {

    private let observers = MulticastDelegate<ReceiptStoreObserver>()

    public enum Error: Swift.Error {
        /// The receipt's URL could not be found, issue a `SKReceiptRefreshRequest` and try again
        case receiptNotFound
        /// No bundle identifier found, make sure you use this code inside an app
        case bundleNotFound
    }

    public let receiptUrl: URL
    public let bundleName: String

    public init(receiptUrl: URL? = nil, bundleName: String? = nil) throws {

        guard let url = receiptUrl ?? Bundle.main.appStoreReceiptURL else {
            // We could check if receipt data is already present, but I'm not sure about it yet
            // check: (try? Data(contentsOf: url))?.isEmpty ?? false
            throw Error.receiptNotFound
        }

        guard let bundle = bundleName ?? Bundle.main.bundleIdentifier else {
            throw Error.bundleNotFound
        }

        self.receiptUrl = url
        self.bundleName = bundle
    }

    public var receipts: [Receipt] {
        return receipt.map { [$0] } ?? []
    }

    public var receipt: Receipt? {
        return receiptData.map {
            Receipt(bundleId: bundleName, receiptData: $0)
        }
    }

    public var receiptData: Data? {
        return try? Data(contentsOf: receiptUrl)
    }

    public func add(receipt: Receipt) -> Bool {
        logWarning("Adding receipts to LocalReceiptStore is not supported!", tag: "LocalReceiptStore")
        return false
    }

    public func removeReceipt() -> Bool {
        logWarning("Removing receipts from LocalReceiptStore is not supported!", tag: "LocalReceiptStore")
        return false
    }

    public func add(observer: ReceiptStoreObserver) {
        observers += observer
    }

    public func remove(observer: ReceiptStoreObserver) {
        observers -= observer
    }
}
