//
//  ReceiptStore.swift
//  StoreMind
//
//  Created by Martin Gratzer on 01/07/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import Foundation

public protocol ReceiptStoreObserver: class {
    func didUpdate(receiptStore: ReceiptStore)
}

public protocol ReceiptStore: class {
    var bundleName: String { get }
    var receipts: [Receipt] { get }

    func add(receipt: Receipt) -> Bool
    func removeReceipt() -> Bool

    func add(observer: ReceiptStoreObserver)
    func remove(observer: ReceiptStoreObserver)
}

public struct Receipt {
    public let bundleId: String
    public let receiptData: Data

    public init(bundleId: String, receiptData: Data) {
        self.bundleId = bundleId
        self.receiptData = receiptData
    }
}

extension Receipt: Equatable, Hashable {
//    public var hashValue: Int {
//        return bundleId.hash & receiptData.hashValue
//    }

    static public func ==(lhs: Receipt, rhs: Receipt) -> Bool {
        return lhs.bundleId == rhs.bundleId
    }
}
