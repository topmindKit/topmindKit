//
//  DiscountTests.swift
//  StoreMindTests
//
//  Created by Martin Gratzer on 10.05.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import XCTest
@testable import StoreMind

final class DiscountTests: XCTestCase {
    
    var sut: Product.Discount!

    func testInit() {
        sut = Product.Discount(price: 123.5,
                               paymentMode: .freeTrial,
                               numberOfPeriods: 2,
                               period: .init(unit: .day, numberOfUnits: 7),
                               locale: Locale(identifier: "en-US"))

        XCTAssertEqual(123.50, sut.price)
        XCTAssertEqual(123500000, sut.priceMicros)
        XCTAssertEqual(.day, sut.period.unit)
        XCTAssertEqual(7, sut.period.numberOfUnits)
        XCTAssertEqual(.freeTrial, sut.paymentMode)
        XCTAssertEqual("$123.50", sut.localizedPrice)
    }
}
