//
//  ProductTests.swift
//  StoreMindTests
//
//  Created by Martin Gratzer on 10.05.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import XCTest
@testable import StoreMind

final class ProductTests: XCTestCase {
    
    var sut: Product!

    override func setUp() {
        super.setUp()
        givenSut()
    }

    func testInit() {

        XCTAssertEqual("A.fixture", sut.id)
        XCTAssertEqual("Fancy Product Fixture", sut.title)
        XCTAssertEqual("Description Fixture", sut.description)
        XCTAssertEqual(123.9, sut.price)
        XCTAssertEqual(123900000, sut.priceMicros)
        XCTAssertEqual("USD", sut.currencyCode)
        XCTAssertEqual(.month, sut.subscriptionPeriod?.unit)
        XCTAssertEqual(6, sut.subscriptionPeriod?.numberOfUnits)        
        XCTAssertEqual("$123.90", sut.localizedPrice)
    }

    func testPriceDivision() {
        XCTAssertEqual(61.95, sut.price(dividedBy: 2))
    }

    func testPriceDivisionByZero() {
        XCTAssertEqual(123.9, sut.price(dividedBy: 0))
    }

    func testPriceDivisionNegative() {
        XCTAssertEqual(61.95, sut.price(dividedBy: -2))
    }

    func testLocalizedPriceDivision() {
        XCTAssertEqual("$61.95", sut.localizedPrice(dividedBy: 2))
    }

    func testLocalizedPriceDivisionByZero() {
        XCTAssertEqual("$123.90", sut.localizedPrice(dividedBy: 0))
    }

    func testLocalizedPriceDivisionNegative() {
        XCTAssertEqual("$61.95", sut.localizedPrice(dividedBy: -2))
    }

    // Mark: Helper

    func givenSut() {
        let locale = Locale(identifier: "en-US")
        let discount = Product.Discount(price: 123.5,
                                        paymentMode: .freeTrial,
                                        numberOfPeriods: 2,
                                        period: .init(unit: .day, numberOfUnits: 7),
                                        locale: locale)

        sut = Product(id: "A.fixture",
                      title: "Fancy Product Fixture",
                      description: "Description Fixture",
                      price: 123.9,
                      subscriptionPeriod: .init(unit: .month, numberOfUnits: 6),
                      discount: discount,
                      locale: locale)
    }
}
