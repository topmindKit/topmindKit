//
//  PurchaseCoordinatorTests.swift
//  StoreMindTests
//
//  Created by Martin Gratzer on 10.05.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import XCTest
import CoreMind
@testable import StoreMind

final class PurchaseCoordinatorTestObserver: PurchaseCoordinatorObserver {

    var expectation: XCTestExpectation?

    var numberOfDidPurchaseCallbacks = 0
    var recentVerificationResult: Verification?
    var numberOfDidRestoreCallbacks = 0
    var recentRestoreError: Error?

    func didPurchase(result: Verification) {
        numberOfDidPurchaseCallbacks += 1
        recentVerificationResult = result
        expectation?.fulfill()
    }

    func didRestore(with error: Error?) {
        numberOfDidRestoreCallbacks += 1
        recentRestoreError = error
        expectation?.fulfill()
    }
}

class PurchaseCoordinatorTests: XCTestCase {

    var observer: PurchaseCoordinatorTestObserver!
    var store: StoreClientMock!
    var verifier: PurchaseVerifierMock!
    var sut: PurchaseCoordinator!

    func givenPurchaseCoordinator() {
        store = StoreClientMock()
        store.products = givenProducts()

        verifier = PurchaseVerifierMock()
        sut = PurchaseCoordinator(client: store, verifier: verifier)

        observer = PurchaseCoordinatorTestObserver()
        sut.add(observer: observer)
    }

    func givenProducts() -> [ProductIdentifier: Product] {
        let identifiers =  ["a", "b", "c", "d"]
        let products = identifiers.map(Product.mock)
        return Dictionary(uniqueKeysWithValues: zip(identifiers, products))
    }

    func doAndWaitForPurchaseCoordinatorCallback(_ expectedFulfillmentCount: UInt = 1, _ block: () throws -> ()) {
        let e = expectation(description: "waitForPurchaseObserverCallback")
        e.expectedFulfillmentCount = Int(expectedFulfillmentCount)
        observer.expectation = e

        do {
            try block()
        } catch {
            XCTFail()
        }
        wait(for: [e], timeout: 1)
    }
}
