//
//  LocalReceiptStoreTests.swift
//  StoreMindTests
//
//  Created by Martin Gratzer on 01/07/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import XCTest
@testable import StoreMind

final class LocalReceiptStoreTests: XCTestCase {

    var sut: LocalReceiptStore!

    let fakeReiceiptUrl: URL = {
        let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return documents.appendingPathComponent("receipt.extension")
    }()

    func testInitWithExplicitParameters() {

        givenLocalReceiptStore(receiptUrl: fakeReiceiptUrl, bundleName: "eu.topmind.test.fixture")

        XCTAssertEqual(fakeReiceiptUrl, sut.receiptUrl)
        XCTAssertEqual("eu.topmind.test.fixture", sut.bundleName)

        let data = try? Data(contentsOf: fakeReiceiptUrl)
        XCTAssertEqual(data, sut.receiptData)
    }

    func testInitWithDefaultParameters() {

        givenLocalReceiptStore() // -> LocalReceiptStore()

        XCTAssertTrue(sut.receiptUrl.absoluteString.hasSuffix("/StoreKit/receipt"))
        XCTAssertEqual("eu.topmind.topmindKitApp", sut.bundleName)
    }

    func testLocalReceiptStoreObservation() {

    }

    // Helper

    func givenLocalReceiptStore(receiptUrl: URL? = nil, bundleName: String? = nil, file: String = #file, line: UInt = #line) {
        do {
            sut = try LocalReceiptStore(receiptUrl: receiptUrl, bundleName: bundleName)
        } catch {
            recordFailure(withDescription: "\(error)", inFile: file, atLine: Int(line), expected: false)
        }
    }
}

final class MockReceiptstoreObserver: ReceiptStoreObserver {

    private(set) var didUpdateCalls = 0

    func didUpdate(receiptStore: ReceiptStore) {
        didUpdateCalls += 1
    }
}
