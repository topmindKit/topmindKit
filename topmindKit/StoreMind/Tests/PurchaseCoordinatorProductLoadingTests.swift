//
//  PurchaseCoordinatorProductLoadingTests.swift
//  StoreMindTests
//
//  Created by Martin Gratzer on 10.05.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import XCTest
import CoreMind
@testable import StoreMind

final class PurchaseCoordinatorProductLoadingTests: PurchaseCoordinatorTests {

    func testProductLoading() {
        givenPurchaseCoordinator()

        let e = expectation(description: "waitProducts")

        sut.loadProducts(for: ["a", "b"]) {
            XCTAssertEqual(["a", "b"], $0.value?.map { $0.id } )
            e.fulfill()
        }

        wait(for: [e], timeout: 1)
    }

    func testProductLoadingWitEmptyInput() {
        givenPurchaseCoordinator()

        let e = expectation(description: "waitProducts")

        sut.loadProducts(for: []) {
            XCTAssertEqual([], $0.value?.map { $0.id } )
            e.fulfill()
        }

        wait(for: [e], timeout: 1)
    }

    func testProductLoadingWithError() {
        givenPurchaseCoordinator()
        store.products = [:]
        store.loadProductsError = "PRODUCT LOADING ERROR"

        let e = expectation(description: "testProductLoadingWithError")

        sut.loadProducts(for: ["a", "b"]) {
            XCTAssertNotNil($0.error)
            e.fulfill()
        }

        wait(for: [e], timeout: 1)
    }

    func testProductLoadingFromCache() {
        givenPurchaseCoordinator()

        let e = expectation(description: "testProductLoadingFromCache1")
        sut.loadProducts(for: ["a", "b"]) {
            XCTAssertEqual(["a", "b"], $0.value?.map { $0.id } )
            // called once (1st callback via "backend")
            e.fulfill()
        }
        // wait because caching happens after the backend returns
        wait(for: [e], timeout: 1)

        let e2 = expectation(description: "testProductLoadingFromCache2")
        // there are 2 hits because sut returns cached results immediately and then goes to the backend for an update
        e2.expectedFulfillmentCount = 2

        sut.loadProducts(for: ["b"]) {
            XCTAssertEqual(["b"], $0.value?.map { $0.id } )
            // called twice (1st callback via cache, 2nd via "backend")
            e2.fulfill()
        }

        wait(for: [e2], timeout: 1)
    }
    
}
