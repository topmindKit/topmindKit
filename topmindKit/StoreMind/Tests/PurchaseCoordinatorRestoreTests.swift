//
//  PurchaseCoordinatorRestoreTests.swift
//  StoreMindTests
//
//  Created by Martin Gratzer on 10.05.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import XCTest
import CoreMind
@testable import StoreMind

final class PurchaseCoordinatorRestoreTests: PurchaseCoordinatorTests {

    override func setUp() {
        super.setUp()
        givenPurchaseCoordinator()
    }

    func testRestoreWithoutRestorablePurchases() {
        store.restoreResult = .success(())

        doAndWaitForPurchaseCoordinatorCallback {
            try sut.restorePurchases()
        }

        XCTAssertNil(observer.recentRestoreError)
        XCTAssertEqual(1, observer.numberOfDidRestoreCallbacks)
        XCTAssertEqual(0, observer.numberOfDidPurchaseCallbacks)
    }

    func testRestoreWithRestorablePurchases() {
        let restored = Purchase.mock(product: "a", quantity: 1, restored: true, date: nil)
        store.restoreTransactionResult = TransactionResult(purchased: [],
                                                           restored: [restored],
                                                           failed: [])

        doAndWaitForPurchaseCoordinatorCallback(2) {
            try sut.restorePurchases()
        }

        XCTAssertNil(observer.recentRestoreError)
        XCTAssertEqual(1, observer.numberOfDidRestoreCallbacks)
        XCTAssertEqual(1, observer.numberOfDidPurchaseCallbacks)
    }

    func testFailingRestore() {
        store.restoreResult = .failure("error")
        doAndWaitForPurchaseCoordinatorCallback {
            try sut.restorePurchases()
        }

        XCTAssertNotNil(observer.recentRestoreError)
        XCTAssertEqual(1, observer.numberOfDidRestoreCallbacks)
        XCTAssertEqual(0, observer.numberOfDidPurchaseCallbacks)
    }
    
}
