//
//  PurchaseCoordinatorPurchaseTests.swift
//  StoreMindTests
//
//  Created by Martin Gratzer on 10.05.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import XCTest
import CoreMind
@testable import StoreMind

final class PurchaseCoordinatorPurchaseTests: PurchaseCoordinatorTests {

    override func setUp() {
        super.setUp()
        givenPurchaseCoordinator()
    }

    /// All Purchases are reported as failures from App Store
    func testPurchaseWithFailingPurchase() {
        givenFailingPurchases()

        doAndWaitForPurchaseCoordinatorCallback {
            try self.sut.purchase(productWith: "a", userIdentifier: nil)
        }

        XCTAssertEqual(1, observer.numberOfDidPurchaseCallbacks)
        XCTAssertEqual(0, observer.numberOfDidRestoreCallbacks)
        XCTAssertEqual(1, store.finishTransactionCalls)

        let verification = observer.recentVerificationResult?.verification
        switch verification {
        case .unknownFailure?: break
        default: XCTFail()
        }
    }

    /// Verifier reports an error which is not related to receipt verification, e.g. error 500
    /// In thit case transactions are not finished -> retry
    func testPurchaseWithUnexpectedValidationResult() {
        givenUnexpectedValidationResult()

        doAndWaitForPurchaseCoordinatorCallback {
            try self.sut.purchase(productWith: "a", userIdentifier: nil)
        }

        XCTAssertEqual(1, observer.numberOfDidPurchaseCallbacks)
        XCTAssertEqual(0, observer.numberOfDidRestoreCallbacks)
        XCTAssertEqual(0, store.finishTransactionCalls)

        let verification = observer.recentVerificationResult?.verification
        switch verification {
        case .unknownFailure?: break
        default: XCTFail()
        }
    }

    /// Verifier reports an error related to receipt validation
    /// In thit case transactions are finished -> no retry, if a problem persists the user has to go and try to restore
    func testPurcahseWithFailedValidation() {
        givenFailedValidation()

        doAndWaitForPurchaseCoordinatorCallback {
            try self.sut.purchase(productWith: "a", userIdentifier: nil)
        }

        XCTAssertEqual(1, observer.numberOfDidPurchaseCallbacks)
        XCTAssertEqual(0, observer.numberOfDidRestoreCallbacks)
        XCTAssertEqual(1, store.finishTransactionCalls)

        let verification = observer.recentVerificationResult?.verification
        switch verification {
        case .validationFailed?: break
        default: XCTFail()
        }
    }

    /// Receipt validation :thumbsup:
    func testPurchaseWithSuccessVerification() {
        givenSuccessfulPurcahses()

        doAndWaitForPurchaseCoordinatorCallback {
            try self.sut.purchase(productWith: "a", userIdentifier: nil)
        }

        XCTAssertEqual(1, observer.numberOfDidPurchaseCallbacks)
        XCTAssertEqual(0, observer.numberOfDidRestoreCallbacks)
        XCTAssertEqual(1, store.finishTransactionCalls)

        let verification = observer.recentVerificationResult?.verification
        switch verification {
        case .success?: break
        default: XCTFail()
        }
    }

    // Mark: Helper

    func givenFailingPurchases() {

        let identifiers =  store.products.map { $0.key }
        let results = identifiers.map {
            TransactionResult(purchased: [],
                              restored: [],
                              failed: [Purchase.Error.init(id: $0, error: "\($0) fixture fail")])
        }

        store.purchaseResult = Dictionary(uniqueKeysWithValues: zip(identifiers, results))

    }

    func givenUnexpectedValidationResult() {
        givenProductPurchaseResult()
        verifier.verificationResult = .unknownFailure("fixture error invalid validation")
    }

    func givenFailedValidation() {
        givenProductPurchaseResult()
        verifier.verificationResult = .validationFailed("fixture error failed validation")
    }

    func givenSuccessfulPurcahses() {
        givenProductPurchaseResult()
        verifier.verificationResult = .success([:])
    }

    func givenProductPurchaseResult() {
        let purchase = Purchase.mock(product: "a")
        store.purchaseResult = ["a": TransactionResult(purchased: [purchase], restored: [], failed: [])]
    }
}
