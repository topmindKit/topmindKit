//
//  PurchaseCoordinatorRefreshReceiptTests.swift
//  StoreMindTests
//
//  Created by Martin Gratzer on 10.05.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import XCTest
import CoreMind
@testable import StoreMind

final class PurchaseCoordinatorRefreshReceiptTests: PurchaseCoordinatorTests {

    override func setUp() {
        super.setUp()
        givenPurchaseCoordinator()
    }

    func testRefreshReceiptOk() {
        store.refreshReceiptError = nil
        sut.refreshReceipt {
            XCTAssertNil($0)
        }
    }

    func testRefreshReceiptFails() {
        store.refreshReceiptError = "Fixture Error"
        sut.refreshReceipt {
            XCTAssertEqual("Fixture Error", $0 as? String)
        }
    }
    
}
