//
//  TransactionResultTests.swift
//  StoreMindTests
//
//  Created by Martin Gratzer on 10.05.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import XCTest
@testable import StoreMind

final class TransactionResultTests: XCTestCase {
    
    var sut: TransactionResult!

    func testEmptyResult() {
        givenEmptyResult()

        XCTAssertFalse(sut.allFailed)
        XCTAssertTrue(sut.isEmpty)
    }

    func testPurchasedResult() {
        givenPurchasedResult()

        XCTAssertFalse(sut.allFailed)
        XCTAssertFalse(sut.isEmpty)
    }

    func testRestoredResult() {
        givenRestoredResult()

        XCTAssertFalse(sut.allFailed)
        XCTAssertFalse(sut.isEmpty)
    }

    func testMixedResult() {
        givenMixedResult()

        XCTAssertFalse(sut.allFailed)
        XCTAssertFalse(sut.isEmpty)
    }

    func testAllFailedResult() {
        givenAllFailedResult()

        XCTAssertTrue(sut.allFailed)
        XCTAssertFalse(sut.isEmpty)
    }

    // Mark: Helper

    func givenEmptyResult() {
        sut = TransactionResult(purchased: [], restored: [], failed: [])
    }

    func givenPurchasedResult() {
        let a = Purchase.mock(product: "A", restored: false)
        let b = Purchase.mock(product: "B", restored: false)
        sut = TransactionResult(purchased: [a, b], restored: [], failed: [])
    }

    func givenRestoredResult() {
        let a = Purchase.mock(product: "A", restored: true)
        let b = Purchase.mock(product: "B", restored: true)
        sut = TransactionResult(purchased: [], restored: [a, b], failed: [])
    }

    func givenMixedResult() {
        let a = Purchase.mock(product: "A", restored: false)
        let b = Purchase.mock(product: "B", restored: true)
        sut = TransactionResult(purchased: [a], restored: [b], failed: [])
    }

    func givenAllFailedResult() {
        sut = TransactionResult(purchased: [], restored: [], failed: [Purchase.Error(id: "test", error: "error")])
    }

}
