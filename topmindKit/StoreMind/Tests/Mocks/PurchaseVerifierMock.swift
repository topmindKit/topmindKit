//
//  PurchaseVerifierMock.swift
//  StoreMindTests
//
//  Created by Martin Gratzer on 10.05.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import Foundation
import CoreMind
import StoreMind

final class PurchaseVerifierMock: PurchaseVerifier {

    let queue = DispatchQueue(label: "PurchaseVerifierMock")

    func verifyReceipt(with result: TransactionResult, completion: @escaping (Verification) -> ()) {
        let verification = verificationResult
        queue.async {
            completion(Verification(transaction: result, verification: verification))
        }
    }

    // mock config
    var verificationResult: Verification.Result = Verification.Result.unknownFailure("nothing defined")
}
