//
//  PurchaseMock.swift
//  StoreMindTests
//
//  Created by Martin Gratzer on 10.05.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import Foundation
@testable import StoreMind

extension Purchase {
    static func mock(product: String, quantity: Int = 1, restored: Bool = false, date: Date? = nil) -> Purchase {
        return Purchase(product: product, quantity: quantity, transaction: Purchase.Transaction(id: "\(product).transaction.id", restored: restored, date: date))
    }
}
