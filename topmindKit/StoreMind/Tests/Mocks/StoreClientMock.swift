//
//  StoreClientMock.swift
//  StoreMindTests
//
//  Created by Martin Gratzer on 10.05.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import Foundation
import CoreMind
@testable import StoreMind

final class StoreClientMock: StoreClient {

    let queue = DispatchQueue(label: "StoreClientMock")

    weak var delegate: StoreClientDelegate?

    var canMakePayments: Bool = true

    func refreshReceipt(completion: @escaping (Error?) -> Void) {
        completion(refreshReceiptError)
    }

    func loadProducts(for identifiers: [ProductIdentifier], completion: @escaping (Result<[Product]>) -> Void) {
        if let error = loadProductsError {
            queue.async {
                completion(.failure(error))
            }
        } else {
            let products = identifiers.compactMap { self.products[$0] }
            queue.async {
                completion(.success(products))
            }
        }
    }

    func purchase(productWith identifier: ProductIdentifier, userIdentifier: String?) throws {
        guard let result = purchaseResult[identifier] else {
            throw "Define a transaction result for \(identifier)."
        }
        queue.async {
            self.delegate?.purchaseResult(result)
        }
    }

    func finishTransactions(result: TransactionResult) {
        finishTransactionCalls += 1
    }

    func restore() throws {
        queue.async {
            self.delegate?.restoreResult(self.restoreResult)

            // restores might be followed by a purchase
            self.queue.async {
                guard let result = self.restoreTransactionResult else {
                    return
                }
                self.delegate?.purchaseResult(result)
            }
        }
    }

    // mock config
    var refreshReceiptError: Error?
    var loadProductsError: Error?
    var products = [ProductIdentifier: Product]()
    var purchaseResult = [ProductIdentifier: TransactionResult]()
    var restoreResult: Result<Void> = .success(())
    var restoreTransactionResult: TransactionResult?
    var finishTransactionCalls = 0
}
