//
//  ProductMock.swift
//  StoreMindTests
//
//  Created by Martin Gratzer on 10.05.18.
//  Copyright © 2018 topmind mobile app solutions. All rights reserved.
//

import Foundation
@testable import StoreMind

extension Product {
    static func mock(id: String) -> Product {
      return Product(id: id,
                     title: "Fixture Title \(id)",
        description: "Fixture Description \(id)",
        price: 10.0,
        subscriptionPeriod: .init(unit: .day, numberOfUnits: 7),
        discount: nil,
        locale: .current)
    }
}
