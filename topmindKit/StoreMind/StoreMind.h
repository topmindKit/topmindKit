//
//  StoreMind.h
//  StoreMind
//
//  Created by Martin Gratzer on 01/07/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

@import Foundation;

#if TARGET_OS_MAC && !TARGET_OS_IPHONE
@import Cocoa;
#else
@import UIKit;
#endif

//! Project version number for StoreMind.
FOUNDATION_EXPORT double StoreMindVersionNumber;

//! Project version string for StoreMind.
FOUNDATION_EXPORT const unsigned char StoreMindVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <StoreMind/PublicHeader.h>


